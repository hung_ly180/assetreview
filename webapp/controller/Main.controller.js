sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/ui/model/json/JSONModel",
		"T180/PerformanceReviewApp/model/models",
		"sap/ui/core/Fragment",
		"sap/m/MessageToast"

	],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller, JSONModel, models, Fragment, MessageToast) {
		"use strict";

		return Controller.extend("T180.PerformanceReviewApp.controller.main", {

			//initialising the models and data
			onInit: function () {},

			onAfterRendering: function () {

				this.getView().setModel(new JSONModel(models.createAssetReviewModelTemplate()), "AssetReviewModel");
				this.getView().setModel(new JSONModel(models.numbersArray()), "Numbers");
				var assetReviewModel = this.getView().getModel("AssetReviewModel");

				var Reviews = assetReviewModel.getProperty("/Reviews");
				var assetStats = assetReviewModel.getProperty("/assetStats");

				//function to search for review
				//starts with assetStats and then searches through Reviews array from assetReviewModel.
				function searchForReviewAndAverage(array, array2) 
				{
					//iterate through the assetStats array.
					array.forEach(function (forEach_assetStats, index) 
					{
						
						//iterate through the Reviews array
						array2.forEach(function (forEach_Reviews, index2) 
						{
							//Checks the asset name and updates the total rating for the asset
							if (forEach_assetStats.AssetName === forEach_Reviews.AssetName) 
							{
								forEach_assetStats.originalSuit = forEach_Reviews.Suitability + forEach_assetStats.originalSuit;
								forEach_assetStats.originalValue = forEach_Reviews.Value + forEach_assetStats.originalValue;
								forEach_assetStats.originalDura = forEach_Reviews.Durability + forEach_assetStats.originalDura;
								forEach_assetStats.originalLonge = forEach_Reviews.Longevity + forEach_assetStats.originalLonge;
								forEach_assetStats.counter++;
								//assetReviewModel.setProperty("/assetStats", assetStats)
							}
						});
						//Update the averages for assetStats to be displayed on the web page.
						forEach_assetStats.aveSuit = (forEach_assetStats.originalSuit / forEach_assetStats.counter).toFixed(2);
						forEach_assetStats.aveValue = (forEach_assetStats.originalValue / forEach_assetStats.counter).toFixed(2);
						forEach_assetStats.aveDura = (forEach_assetStats.originalDura / forEach_assetStats.counter).toFixed(2);
						forEach_assetStats.aveLonge = (forEach_assetStats.originalLonge / forEach_assetStats.counter).toFixed(2);
						assetReviewModel.setProperty("/assetStats", assetStats);
						
					});
				}
				searchForReviewAndAverage(assetStats, Reviews);
			},

			//TEST FUNCTION
			//to see whats inside the model
			test: function () {
				console.log(this.getView().getModel("AssetReviewModel").getProperty("/newReview"));
			},

			//when user clicks on add review
			onClickAddReviewButton: function () {
				var oView = this.getView();
				this.getView().getModel("AssetReviewModel").setProperty("/newReview", models.createNewReviewTemplate());

				//console.log(this.getView().getModel("AssetReviewModel").getProperty("/newReview"));
				//if the addReviewDialog doesn't exist, then create it and open
				if (!this.byId("addReviewDialog")) {

					//load fragment
					Fragment.load({
						id: oView.getId(),
						name: "T180.PerformanceReviewApp.view.addReview",
						controller: this
					}).then(function (oDialog) {
						//connect dialog
						oView.addDependent(oDialog);
						oDialog.open();
					});
				}
				//addReviewDialog already exists
				else {
					this.byId("addReviewDialog").open();
				}
			},

			//closing the add review dialog on cancel
			onAddReviewDialogClose: function () {
				//to reset the fields and close dialogue box
				this.byId("addReviewDialog").close();
				this.byId("addReviewDialog").destroy();
			},

			//submitting the review
			onReviewSubmit: function () {
				var assetReviewModel = this.getView().getModel("AssetReviewModel");
				var newReview = assetReviewModel.getProperty("/newReview");
				var assetStats = assetReviewModel.getProperty("/assetStats");

				//Getting the Reviews array from the assetReviewModel, and inserting the submitted review
				var reviewsArray = assetReviewModel.getProperty("/Reviews");
				reviewsArray.push(assetReviewModel.getProperty("/newReview"));

				// Write the updated array back to the model
				this.getView().getModel("AssetReviewModel").setProperty("/Reviews", reviewsArray);

				//console.log(assetReviewModel.getData());

				//update averages
				//function to search for review
				//starts with assetStats and then searches through Reviews array from assetReviewModel.
				function searchForReviewAndAverage(array, array2) 
				{
					array.forEach(function (forEach_assetStats, index) 
					{

						//checks which asset was recently reviewed 
						if (forEach_assetStats.AssetName === newReview.AssetName) 
						{
							
							//adding the values from the new review to the orignal values of the newly reviewed asset
							forEach_assetStats.originalSuit = parseInt(newReview.Suitability,10) + parseInt(forEach_assetStats.originalSuit,10);
							forEach_assetStats.originalValue = parseInt(newReview.Value,10) + parseInt(forEach_assetStats.originalValue,10);
							forEach_assetStats.originalDura = parseInt(newReview.Durability,10) + parseInt(forEach_assetStats.originalDura,10);
							forEach_assetStats.originalLonge = parseInt(newReview.Longevity,10) + parseInt(forEach_assetStats.originalLonge,10);
							forEach_assetStats.counter++;
							assetReviewModel.setProperty("/assetStats", assetStats);

						}
						//Update the averages for assetStats to be displayed on the web page.
							//calculating the average
							forEach_assetStats.aveSuit = (forEach_assetStats.originalSuit / forEach_assetStats.counter).toFixed(2);
							forEach_assetStats.aveValue = (forEach_assetStats.originalValue / forEach_assetStats.counter).toFixed(2);
							forEach_assetStats.aveDura = (forEach_assetStats.originalDura / forEach_assetStats.counter).toFixed(2);
							forEach_assetStats.aveLonge = (forEach_assetStats.originalLonge / forEach_assetStats.counter).toFixed(2);
							assetReviewModel.setProperty("/assetStats", assetStats);
						//console.log(forEach_assetStats.counter+" Reviews for "+forEach_assetStats.AssetName);
					});
					
				}
				assetReviewModel.setProperty("/assetStats", assetStats);
				searchForReviewAndAverage(assetStats, newReview);


				//close the dialog box and reset it
				this.byId("addReviewDialog").close();
				MessageToast.show("Review Added and Averages Updated!");
				this.byId("addReviewDialog").destroy();
			},

			//To control the number of columns for the cardspace
			onSlider: function (oEvent) {
				var fValue = oEvent.getParameter("value");
				this.byId("gridListPanel").setWidth(fValue + "%");
			}
		});
	});